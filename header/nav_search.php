<?php
    if ($check_session) {
        $data = $userRow_data[0];
    }
?> 
<div class="container">
    <div class="row">

        <nav class="navbar navbar-light bg-faded">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-right">
                    <?php
                    
                        $navigation = $mydatabase->myquery("select * from navigation where status > 0 and type != 'side'");

                        foreach ($navigation as $value) {
                            echo '<li><a style="text-align: center; border: 1px solid #eee;border-left: none;border-bottom:none;" href="' . $value['url'] . '" class="menu-link">' . $value['nama'] . '</a></li>';
                        }
                    ?>
                </ul>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">

                    <?php
                        // LINK TULIS POST
//                        if ($check_session) {
//                            echo ' <li><a href="#" class="menu-link" data-toggle="modal" data-target="#post-modal">TULIS POST</a></li>';
//                        }
                    ?>
                    <li style="text-align:center;" class="dropdown">
                        <?php
//                                          

                            if ($check_session) {
                                echo ' <a style="    text-transform: uppercase; font-weight: bold;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            &nbsp;Selamat Datang&nbsp;&nbsp;';
                                echo $data['username'] . '&nbsp;<span class="caret"></span></a>';
                            }
                            else {
                                echo ' <a class="" style="text-transform: uppercase; font-weight: bold;" href="http://' . $_SERVER["SERVER_NAME"] . '/Tugas_akhir/member/" >Login</a>';
                            }
                        ?>

                        <ul class="dropdown-menu">
                            <?php
                                if ($check_session) {
                                    echo '<li><a href="http://' . $_SERVER["SERVER_NAME"] . '/Tugas_akhir/member/profil/ ">&nbsp;Profilku</a></li>';
                                }
                            ?>
                            <?php
                                if ($check_session) {
                                    echo '<li><a href="http://' . $_SERVER["SERVER_NAME"] . '/Tugas_akhir/member/logout.php?logout ">&nbsp;Logout</a></li>';
                                }
                            ?>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->

        </nav> 
    </div>
</div>

<div class="modal fade" id="post-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">


    <div class="modal-dialog">

        <div class="login modal-container">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h1>Buat Keluhan Anda.</h1><br>
            <div id="message"></div>
            <form id="uploadimage" enctype="multipart/form-data" action="" method="post">
                <div class="form-group">
                    <label for="pwd">Judul :</label>
                    <input type="text" class="form-control" name="title" placeholder="Judul Keluhan">

                    <div id="image_preview">
                        <img id="previewing">
                    </div>
                    <label for="pwd">Gambar :</label>
                    <input style="margin-bottom:10px;" type="file" name="file" id="file" required />
                    <label for="pwd">Deskripsi :</label>
                    <textarea name="deskripsi" class="form-control" placeholder="Tambah Keterangan Keluhan"></textarea>


                </div>
                <input type="submit" id="submit_post" name="login" class="login loginmodal-submit" value="Kirim">
            </form>

        </div>
    </div>
</div>