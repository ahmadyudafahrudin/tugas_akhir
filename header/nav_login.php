    <div class="row">

        <nav class="navbar navbar-light bg-faded">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="nav navbar-nav navbar-right">
                    <?php
                    
                        $navigation = $mydatabase->myquery("select * from navigation where status > 0 and type != 'side'");

                        foreach ($navigation as $value) {
                            echo '<li><a style="text-align: center; border: 1px solid #eee;border-left: none;border-bottom:none;" href="' . $value['url'] . '" class="menu-link">' . $value['nama'] . '</a></li>';
                        }
                    ?>
                </ul>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

            </div><!--/.nav-collapse -->

        </nav> 

 
    </div>

