<?php
    session_start();
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');


// select loggedin users detail
    $check_session = $mydatabase->check_session();
    if ($check_session) {
        $userRow_data = $mydatabase->user_log($_SESSION['user']);
        $userRowProfil = $userRow_data[0];
    }

//Query post
    $q = "SELECT mp.*, c.nama,c.alamat FROM post mp
inner join company c
on c.company_id = mp.company_id
where mp.status > 0 order by mp.member_post_id DESC";
    $myresult = $mydatabase->myquery($q);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Selamat Datang </title>
        <?php include HEAD_SETTING; ?>
    </head>
    <body>

        <?php include NAVIGATION; ?>

        <div id="wrapper">

            <div style="background-color: #fff; padding-bottom: 50px"class="container">

                <div class="page-header margin-top-10">
                    <div class="row"> 
                        <div class="col-md-6 text-align-md">   	<h3 style="font-family: Roboto,sans-serif;
                                                                    font-weight: bold;">Daftar lowongan kerja 2017</h3></div>

                        <div style="text-align: right;" class="hidden-xs hidden-sm  col-md-6 margin-top-10">
                            <div class="row">
                                <form class="navbar-form" action="search/" method="GET" role="search">
                                    <div class="input-group add-on">
                                        <input class="form-control" placeholder="Search" name="q" id="srch-term" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-10 main-bar">
                    <div class="main-content">

                        <?php
// Content
                            foreach ($myresult as $key => $userPost) {
                                ?>
                                <div style="border:1px solid #f3f5f6;border-radius: 1%;" class=" grid qlue-content"> 
                                    <div class="wrapper-qlue">

                                        <div style="text-align: center; margin-bottom: 10px;" class="head-clue">
                                            <div class="row">

                                                <div class="col-md-9"> 
                                                    <div style="padding:5px; padding-bottom: 0px; text-align:left ;font-family: 'Roboto',sans-serif;font-size: 25px;" class="title-qlue">   <?php echo $userPost['post_title']; ?> </div>
                                                    <div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 15px; text-align: left; color: green" class="title-qlue"> Gaji :   Rp. 3.400.000,- </div>

                                                    <div class="wrapper_profil" style="    height: 50px;
                                                         margin: 5px;
                                                         text-align: left;
                                                         ">
                                                        <div style="padding: 5px;
                                                             margin-top: 5px;
                                                             padding-bottom: 0px;
                                                             text-align: left;
                                                             font-family: 'Roboto',sans-serif;
                                                             font-size: 15px;
                                                             text-align: left;
                                                             font-weight: bold;" class="name_profil">  <?php echo $userPost['nama']; ?> </div>
                                                        <div style="padding: 5px;
                                                             margin-top: -5px;
                                                             padding-top: : 0px;
                                                             text-align: left;
                                                             font-family: 'Roboto',sans-serif;
                                                             font-size: 12px;
                                                             text-align: left;
                                                             color: gray;"class="addres_profil">  <?php echo $userPost['alamat']; ?> </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-3 col-md-3">
                                                    <div style="width: 50px;

                                                         height: 50px;   
                                                         margin: 5px;"class="img-profil">
                                                        <a href="">    <img style="width: 80px;height: 80px;"
                                                            <?php
                                                            if ($userPost['post_img'] == NULL) {
                                                                $img_prof = "http://" . HOMES ."/media/img/img_profil/icon_not_found.png";
                                                            }
                                                            else {
                                                                $img_prof = "http://" . HOMES ."/" . $userPost['post_img'];
                                                            }
                                                            ?>src="<?php echo $img_prof; ?>">
                                                        </a>
                                                    </div></div>


                                            </div>    
                                        </div>

                                    </div>
                                    <div class="img-clue">
                                        <!--<img style="width: 100%;"src="--> 
                                        <?php
//                                                if ($userPost['post_img'] == NULL) {
//                                                    $img_prof = "./media/img/img_post/image-not-found.png";
//                                                }
//                                                else {
//                                                    $img_prof = $userPost['post_img'];
//                                                }
                                        ?><?php // echo $img_prof; ?> 
                                        <!--">-->
                                    </div>
                                                                                <!--<div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 14px;" class="desc-clue"> <?php echo $userPost['post_desc']; ?></div>-->
                                    <div style="padding: 5px;padding-bottom: 5px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 14px;" class="desc-clue">
                                        <a style="float:right; margin-bottom: 5px;"class="btn btn-primary" href="<?php echo 'http://' . HOMES . '/post/' . $userPost['post_url'] . '/'; ?>">
                                            Lanjutkan melamar <li class="glyphicon glyphicon-hand-right"></li>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
// End dari content
                        ?>

                    </div>

                </div>
                <div class="col-lg-2 side-bar">
                    <div class="row">
                        <div class="col-md-k12">
                            <?php
                                $cms_side_page = "select * from cms_side where status > 0 order by cms_side_id DESC";
                                $cms_side_page_result = $mydatabase->myquery($cms_side_page);
                                foreach ($cms_side_page_result as $key_values => $value_side) {
                                    ?>
                                    <div id="feedzy_wp_widget-2" class="sidebar-widget widget_feedzy_wp_widget">
                                        <h3 style="font-weight: bold"class="widget-head"><?php echo $value_side['nama']; ?></h3><div class="feedzy-rss"><ul>
                                                <?php
                                                if ($value_side['type_content'] == 'Side') {

                                                    $side_page = "select * from navigation where status > 0 and type='side' ";
                                                    $side_page_myresult = $mydatabase->myquery($side_page);

                                                    foreach ($side_page_myresult as $key => $value) {
                                                        ?>
                                                        <li style="padding: 6px 0 10px" class="rss_item">
                                                            <span class="title">

                                                                <a href="<?php echo $value['url'] ?>" target="_blank">
                                                                    <?php echo ucwords($value['nama']); ?>
                                                                </a>

                                                            </span>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                elseif ($value_side['type_content'] == 'Html') {
                                                    $side_page = "select * from cms_side where status > 0 and type_content='Html' ";
                                                    $side_page_myresult = $mydatabase->myquery($side_page);
                                                    foreach ($side_page_myresult as $key_side => $value_side) {
                                                        ?>
                                                        <li style="padding: 6px 0 10px" class="rss_item">
                                                            <span>

                                                                <?php echo $value_side['html']; ?>

                                                            </span>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul> 
                                        </div>        
                                    </div>

                                <?php } ?>

                        </div>
                    </div>
                </div>

            </div>

        </div>

        <?php include FOOTER; ?>

    </body>
</html>
