<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
$check_session = $mydatabase->check_session();

$error = 0;
$errorName = '';
$errorEmail = '';
$errorPass = '';
$errorNamefull = '';
$errorAlamat = '';
$name = '';
$email = '';

if (isset($_POST['btn-signup'])) {

    // clean user inputs to prevent sql injections
    $name = trim($_POST['name']);
    $name = strip_tags($name);
    $name = htmlspecialchars($name);

    $namefull = trim($_POST['namefull']);
    $namefull = strip_tags($namefull);
    $namefull = htmlspecialchars($namefull);

    $alamat = trim($_POST['alamat']);
    $alamat = strip_tags($alamat);
    $alamat = htmlspecialchars($alamat);

    $email = trim($_POST['email']);
    $email = strip_tags($email);
    $email = htmlspecialchars($email);

    $pass = trim($_POST['pass']);
    $pass = strip_tags($pass);
    $pass = htmlspecialchars($pass);

    // basic name validation
    if (empty($name)) {
        $error = 1;
        $errorName = "Masukan nama anda...";
    } else if (strlen($name) < 3) {
        $error = 1;
        $errorName = "Nama harus diatas 3 karakter";
    } else if (!preg_match("/^[a-zA-Z ]+$/", $name)) {
        $error = 1;
        $errorName = "Nama harus Abjad dan Huruf..";
    }

    // basic name validation
    if (empty($namefull)) {
        $error = 1;
        $errorNamefull = "Masukan nama anda...";
    } else if (strlen($namefull) < 3) {
        $error = 1;
        $errorNamefull = "Nama harus diatas 3 karakter";
    } else if (!preg_match("/^[a-zA-Z ]+$/", $namefull)) {
        $error = 1;
        $errorNamefull = "Nama harus Abjad dan Huruf..";
    }

    // basic name validation
    if (empty($alamat)) {
        $error = 1;
        $errorAlamat = "Masukan alamat anda...";
    } else if (strlen($alamat) < 3) {
        $error = 1;
        $errorAlamat = "Alamat harus diatas 3 karakter";
    } else if (!preg_match("/^[a-zA-Z ]+$/", $alamat)) {
        $error = 1;
        $errorAlamat = "Alamat harus Abjad dan Huruf..";
    }

    //basic email validation
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = 1;
        $errorEmail = "Masukan Email yang benar...";
    } else {
        // check email exist or not
        $query = "SELECT email FROM member_detail WHERE email='$email'";
        $result = $mydatabase->myquery($query);
        $count = count($result);
        if ($count != 0) {
            $error = 1;
            $errorEmail = "Email Sudah digunakan...";
        }
    }
    // password validation
    if (empty($pass)) {
        $error = 1;
        $errorPass = "Password tidak boleh kosong...";
    } else if (strlen($pass) < 6) {
        $error = 1;
        $errorPass = "Password harus diatas 6 karakter...";
    }

    // Jika tidak ada error
    if ($error != 1) {

        $query = "INSERT INTO member_detail (nama, alamat, email) VALUES ('" . $namefull . "','" . $alamat . "','" . $email . "')";
        $query_2 = "INSERT INTO member (username,password) VALUES ('" . $name . "','" . $pass . "')";
        $select_id = "select id from member where username = '" . $name . "'";

        $result = $mydatabase->myinsert($query);
        $result = $mydatabase->myinsert($query_2);        
        $result2 = $mydatabase->myquery($select_id);
        $result3 = $mydatabase->carr_get($result2, 'id');
        
        if ($result) {
            
            $mydatabase->create_folder(ROOT.'/media/img/img_post/',$result3);
            $mydatabase->create_folder(ROOT.'/media/img/img_profil/',$result3);

            $errorType = "success";
            $errMSG = "Registrasi telah berhasil...";
            unset($name);
            unset($email);
            unset($pass);
        } else {
            $errorType = "danger";
            $errMSG = "Registrasi gagal...";
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Member</title>
        <?php include HEAD_SETTING; ?>
    </head>
    <body>

        <div class="container" style="background-color: #fff">
<?php include NAVIGATION_LOGIN; ?>
            <img width="100%" style="margin-bottom:250px;" src="<?php echo 'http://' . HOMES . '/media/img/logo/' . LOGO; ?>">
            <div style="margin:1% auto;" id="login-form">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">

                    <div class="col-md-12">

                        <div class="form-group">
                            <h2 class="">Registrasi</h2>
                        </div>

                        <div class="form-group">
                            <hr>
                        </div>

                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="form-group">
                                <div class="alert alert-<?php echo ($errTyp == "success") ? "success" : $errTyp; ?>">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" name="name" class="form-control" placeholder="Masukan Username..." maxlength="50" />
                            </div>
                            <span class="text-danger"><?php echo $errorName; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" name="namefull" class="form-control" placeholder="Masukan Nama Lengap..." maxlength="50" />
                            </div>
                            <span class="text-danger"><?php echo $errorNamefull; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" name="alamat" class="form-control" placeholder="Masukan Alamat anda..." maxlength="50" />
                            </div>
                            <span class="text-danger"><?php echo $errorAlamat; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="email" name="email" class="form-control" placeholder="Masukan Email Anda..." maxlength="40" />
                            </div>
                            <span class="text-danger"><?php echo $errorEmail; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="pass" class="form-control" placeholder="Masukan Password Anda..." maxlength="15" />
                            </div>
                            <span class="text-danger"><?php echo $errorPass ?></span>
                        </div>

                        <div class="form-group">
                            <hr>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary" name="btn-signup">Daftar</button>
                        </div>

                        <div class="form-group">
                            <hr>
                        </div>

                        <div class="form-group">
                            <a href="index.php">Klik disini untuk login</a>
                        </div>

                    </div>

                </form>
            </div>	

        </div>
<?php include FOOTER; ?>
    </body>
</html>