<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// it will never let you open index(login) page if session is set
if (isset($_SESSION['user']) != "") {
    header("Location: http://" . HOMES . "/Tugas_akhir/");
    exit;
}

// Mendefinisikan variable
$usernameError = '';
$passError = '';
$error = 0;

if (isset($_POST['btn-login'])) {

    // prevent sql injections/ clear user invalid inputs
    $username = trim($_POST['username']);
    $username = strip_tags($username);
    $username = htmlspecialchars($username);

    $pass = trim($_POST['pass']);
    $pass = strip_tags($pass);
    $pass = htmlspecialchars($pass);
    // prevent sql injections / clear user invalid inputs

    if (empty($username)) {
        $error = 1;
        $usernameError = "Masukan Username Anda...";
    }

    if (empty($pass)) {
        $error = 1;
        $passError = "Masukan Password anda...";
    }



    // if there's no error, continue to login
    if ($error == 0) {

// select loggedin users detail
        $row = $mydatabase->user_log($username);
// if uname/pass correct it returns must be 1 row
        $count = count($row);
        if ($count > 0) {
            $row = $row[0];
        }


        if ($count > 0 && $row['password'] == $pass) {
            $_SESSION['user'] = $row['username'];

            header("Location: http://" . HOMES . "/Tugas_akhir/");
        } else {
            $errMSG = "Salah memasukan password / username...";
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login member</title>
        <?php include HEAD_SETTING; ?>
        
    </head>
    <body>
        <div style="background-color: #fff;" class="container">
            <?php include NAVIGATION_LOGIN; ?>
            <img width="100%" style="margin-bottom:50px;" src="<?php echo 'http://' . HOMES .'/media/img/logo/' . LOGO; ?>">
            <div id="login-form">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">

                    <div class="col-md-12">

                        <div class="form-group">
                            <h2 class="">Sign In.</h2>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <?php
                        if (isset($errMSG)) {
                            ?>
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                                <input type="text" name="username" class="form-control" placeholder="Username anda" maxlength="40" />
                            </div>
                            <span class="text-danger"><?php echo $usernameError; ?></span>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="pass" class="form-control" placeholder="Password anda" maxlength="15" />
                            </div>
                            <span class="text-danger"><?php echo $passError; ?></span>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-primary" name="btn-login">Sign In</button>
                        </div>

                        <div class="form-group">
                            <hr />
                        </div>

                        <div class="form-group">
                            <a href="register.php">Klik untuk daftar...</a>
                        </div>

                    </div>

                </form>
            </div>	

        </div>
<?php include FOOTER; ?>
    </body>
</html>
