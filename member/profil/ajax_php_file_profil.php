<?php

session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
$userRow = $mydatabase->user_log($_SESSION['user']);
$userRow = $userRow[0];

if (isset($_FILES["file"]["type"]) || isset($_POST)) {

    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
            ) && ($_FILES["file"]["size"] < 10000000)//Approx. 1000kb files can be uploaded.
            && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("./media/img/img_profil/" . $userRow['id'] . "/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
            } else {
                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
                $targetPath = $_SERVER["DOCUMENT_ROOT"] . "/Tugas_akhir/media/img/img_profil/" . $userRow['id'] . "/" . $_FILES['file']['name']; // Target path where file is to be stored

                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file

                echo "<div class='alert alert-success'> <strong>Berhasil!</strong> Data berhasil di upload.</div>";

                $nama = $_POST['names'];
                $ktp = $_POST['ktp'];
                $alamat = $_POST['alamat'];
                $id = $userRow['id'];
                $targetPath_fix = "media/img/img_profil/" . $userRow['id'] . "/" . $_FILES['file']['name'];
                $ket = $_POST['keterangan'];
                
                echo "<div class='alert alert-success'> <strong>Berhasil!</strong> Data berhasil di upload.</div>";
                try {
                    $result = "UPDATE member_detail set nama ='" . $nama . "', alamat ='" . $alamat . "',keterangan='" . $ket . "',img_profil='" . $targetPath_fix . "',ktp='" . $ktp . "' where member_detail_id ='" . $id . "' ";

                    $mydatabase->myinsert($result);
                } catch (Exception $e) {
                    echo 'Kesalahan : ', $e->getMessage(), "\n";
                }
            }
        }
    } else {
        echo "<div class='alert alert-danger'> <strong>Gagal!</strong>Data gagal dimasukan.</div>";
    }
} else {
    die('nothing to do... -_-');
}
?>