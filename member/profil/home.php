<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
    $check_session = $mydatabase->check_session();
    if ($check_session) {
        $userRow_data = $mydatabase->user_log($_SESSION['user']);
    }
    
// select loggedin users detail
$res =  "SELECT * FROM member "
        . "inner join 
member_detail on member_detail.member_detail_id = member.id "
        . "WHERE member.username='" . $_SESSION['user'] . "'";

$userRow = $mydatabase->myquery($res);
$userRow = $userRow[0];
$userRowProfil = $userRow_data[0];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Selamat Datang </title>
        <?php include HEAD_SETTING;?>
    </head>
    <body>

      <?php include NAVIGATION;?>

        <div id="wrapper">


            <div style="background-color: #ffffff; padding-bottom: 50px"class="container">

                <div class="page-header margin-top-10">
                    <div class="row"> <div class="col-md-6 text-align-md">   	<h3 style="font-family: Roboto,sans-serif;
                                                                                  font-weight: bold;">My Profil</h3></div>

                        <div style="text-align: right;" class="hidden-xs col-md-6 margin-top-10 text-align-md">
                            <button type="button" class="btn btn-standart" data-toggle="modal" data-target="#login-modal" >Edit Profil</button>
                            <button type="button" id="mylist" class="btn btn-standart">Lihat lamaran kerja  </button>

                        </div>
                    </div>
                </div>

                <div class="row" id="mycontont">
                    <div class="col-lg-12">
                        <div style="    margin: auto;" class="main-content">


                            <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">


                                <div class="modal-dialog">

                                    <div class="loginmodal-container">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h1>Edit Profil</h1><br>
                                        <div id="message2"></div>
                                        <form id="uploadimage2" enctype="multipart/form-data" action="" method="post">
                                            <div class="form-group">
                                                <label for="pwd">Nama :</label>
                                                <input type="text" class="form-control" name="names" placeholder="Isikan nama">

                                                <div id="image_preview">
                                                    <img id="previewing">
                                                </div>
                                                <label for="pwd">Upload Gambar :</label>
                                                <input style="margin-bottom:10px;" type="file" name="file" id="file" required />
                                                <label for="pwd">KTP :</label>
                                                <input type="text" name="ktp" class="form-control" placeholder="Tambah Keterangan KTP">

                                                <label for="pwd">Alamat :</label>
                                                <textarea type="text" name="alamat" class="form-control" placeholder="Tambah Keterangan Alamat"></textarea>
                                                <label for="pwd">Keterangan :</label>
                                                <textarea type="text" name="keterangan" class="form-control" placeholder="Tambah Keterangan"></textarea>


                                            </div>
                                            <input type="submit" id="submit_post" name="login" class="login loginmodal-submit" value="Kirim">
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 qlue-content"> 
                                <div style="border:1px solid #f3f5f6;border-radius: 1%;"class="wrapper-qlue">

                                    <div style="text-align: center; margin-bottom: 10px;" class="head-clue">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3"><div style="margin: 5px;"class="img-profil"><img style="width: 100px;" src="<?php
                                                    if ($userRow['img_profil'] == NULL) {
                                                        $userRow['img_profil'] = 'media/img/img_profil/icon_not_found.png';
                                                    }

                                                    echo 'http://' . $_SERVER["SERVER_NAME"] . '/Tugas_akhir/' . $userRow['img_profil'];
                                                    ?>"></div></div>
                                            <div class="col-xs-12 col-md-9"> 
                                                <div class="wrapper_profil" style=" text-align: left;">
                                                    <div style="padding: 5px;
                                                         margin-top: 5px;
                                                         padding-bottom: 0px;
                                                         text-align: left;
                                                         font-family: 'Roboto',sans-serif;
                                                         font-size: 30px;
                                                         text-align: left;
                                                         font-weight: bold;" class="name_profil"> <?php echo $userRow['nama']; ?> </div>
                                                    <div style="padding: 5px;
                                                         margin-top: -5px;
                                                         padding-top: : 0px;
                                                         text-align: left;
                                                         font-family: 'Roboto',sans-serif;
                                                         font-size: 17px;
                                                         text-align: left;
                                                         color: gray;"class="addres_profil">   <?php echo $userRow['KTP']; ?> </div>
                                                    <div style="padding: 5px;
                                                         margin-top: -5px;
                                                         padding-top: : 0px;
                                                         text-align: left;
                                                         font-family: 'Roboto',sans-serif;
                                                         font-size: 17px;
                                                         text-align: left;
                                                         color: gray;
                                                         "class="addres_profil"> Alamat : <?php echo $userRow['alamat']; ?> </div>
                                                    <div style="padding: 5px;
                                                         margin-top: -5px;
                                                         padding-top: : 0px;
                                                         text-align: left;
                                                         font-family: 'Roboto',sans-serif;
                                                         font-size: 20px;
                                                         text-align: left;
                                                         "class="addres_profil"> <?php echo $userRow['keterangan']; ?> </div>
                                                </div>
                                            </div>

                                        </div>    


                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        
 <?php include FOOTER; ?>

    </body>
</html>
