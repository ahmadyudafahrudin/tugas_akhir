<?php

    /*
     * Author : @taichu
     * Since : Sabtu, 07-07-2017
     * Description : This file is configuration of all setting
     */

    class mydatabase {

        public $db;

        public function __construct() {
            try {
                // Buat Object PDO baru dan simpan ke variable $db
                $this->db = new PDO('mysql:host=' . DBHOST . ';dbname=tugas_akhir', DBUSER, DBPASS);
                // Mengatur Error Mode di PDO untuk segera menampilkan exception ketika ada kesalahan
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $exception) {
                die("Connection error: " . $exception->getMessage());
            }

            //--------------------- ADDITIONAL BANNER ---------------------//
            $mybanner = $this->myquery("select nama from banner");
            $nama_banner = $this->carr_get($mybanner, 'nama');
            define('BANNER', $nama_banner);
            //--------------------- BANNER ---------------------//
        }

// MYSQL CONNECT
        public function user_log($param) {
            // Buat prepared statement untuk mengambil semua data dari tbBiodata
            $query = $this->db->prepare("SELECT * FROM member WHERE username='" . $param . "' and status > 0");
            // Jalankan perintah SQL
            $query->execute();
            // Ambil semua data dan masukkan ke variable $data
            $data = $query->fetchAll();
            return $data;
        }

        public function admin_log($param) {
            // Buat prepared statement untuk mengambil semua data dari tbBiodata
            $query = $this->db->prepare("SELECT * FROM admin WHERE username='" . $param . "'");
            // Jalankan perintah SQL
            $query->execute();
            // Ambil semua data dan masukkan ke variable $data
            $data = $query->fetchAll();
            return $data;
        }

        public function check_session() {
            if (isset($_SESSION['user'])) {
                return TRUE;
            }
            else {
                return FALSE;
            }
        }

        public function admin_session() {
            if (!isset($_SESSION['admin'])) {
                header("Location: http://" . HOMES . "/admins/login/");
                exit;
            }
        }

        public function admin_check_session() {
            if (isset($_SESSION['admin'])) {
                return TRUE;
            }
            else {
                $this->admin_session();
            }
        }

        public function session() {
            if (!isset($_SESSION['user'])) {
                header("Location: http://" . HOMES . "/member/index.php");
                exit;
            }
        }

        public function myquery($param) {
            // Buat prepared statement untuk mengambil semua data dari tbBiodata
            $query = $this->db->prepare($param);
            // Jalankan perintah SQL
            $query->execute();
            // Ambil semua data dan masukkan ke variable $data
            $data = $query->fetchAll();
            return $data;
        }

        public function myinsert($param) {
            $bool = FALSE;
            try {
                $query = $this->db->prepare($param);
                $query->execute();
                $bool = TRUE;
            }
            catch (Exception $e) {
                
            }
            return $bool;
        }

        public function carr_get($array = array(), $key_index = '') {
            try {
                foreach ($array as $key => $value) {
                    return $value[$key_index];
                }
            }
            catch (Exception $e) {
                echo 'Bukan array atau key' . $key_index . 'tidak ditemukan';
            }
        }

        public function create_folder($path, $folder_name) {
            try {
                mkdir($path . $folder_name, 0755, true);
            }
            catch (Exception $e) {
                echo 'message' . $e->getMessage();
            }
        }

        public function get_code_post($post_url = '') {
//            $path = 'http://' . HOMES . '/' . FOLDER_FILE . '/post/' . $post_url . '/code.txt';
            $str = @file_get_contents('code.txt');
            if ($str === FALSE) {
                throw new Exception("Cannot access '$path' to read contents.");
            }
            else {
                return $str;
            }
        }

        public function mycleansql($param = '') {
            $sql_safe = mysql_real_escape_string($param);
            return $sql_safe;
        }

    }

    return $mydatabase = new mydatabase();


    