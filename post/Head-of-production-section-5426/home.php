<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
$check_session = $mydatabase->check_session();
if ($check_session) {
    $userRow_data = $mydatabase->user_log($_SESSION['user']);
    $userRowProfil = $userRow_data[0];
}


// Get code POST
$code = $mydatabase->get_code_post();

//Query post
$q = "SELECT mp.*, c.nama,c.alamat FROM post mp
inner join company c
on c.company_id = mp.company_id
where mp.status > 0 and mp.member_post_id = " . $code . " order by mp.member_post_id DESC";

$q_apply = "SELECT ap.* FROM post_apply ap
inner join post p on p.member_post_id = ap.member_post_id
inner join member_detail md on md.member_detail_id = ap.member_detail_id
where ap.status > 0 and ap.member_post_id = " . $code;
if ($check_session) {
    $q_apply .= " and ap.member_detail_id=" . $mydatabase->carr_get($userRow_data, 'id') . " order by ap.post_apply_id DESC";
}
$myresult = $mydatabase->myquery($q);
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $mydatabase->carr_get($myresult, 'post_title'); ?> </title>
        <?php include HEAD_SETTING; ?>
    </head>
    <body>

        <?php include NAVIGATION; ?>

        <div id="wrapper">

            <div style="background-color: #fff; padding-bottom: 50px"class="container">

                <div class="page-header margin-top-10">
                    <div class="row"> <div class="col-md-6 text-align-md">   	<h3 style="font-family: Roboto,sans-serif;
                                                                                  font-weight: bold;">Daftar lowongan kerja 2017</h3></div>

                        <div style="text-align: right;" class="hidden-xs hidden-sm  col-md-6 margin-top-10">
                            <div class="row">
                                <form class="navbar-form" role="search">
                                    <div class="input-group add-on">
                                        <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10 main-bar">
                        <div class="main-content">
                            <div class="row">
                                <?php
// Content
                                foreach ($myresult as $key => $userPost) {
                                    ?>
                                    <div class="col-md-12 qlue-content"> 
                                        <div style="border:1px solid #f3f5f6;border-radius: 1%;"class="wrapper-qlue">

                                            <div style="text-align: center; margin-bottom: 10px;" class="head-clue">
                                                <div class="row">
                                                    <div class="col-xs-3 col-md-1"><div style="width: 50px;

                                                                                        height: 50px;   
                                                                                        margin: 5px;"class="img-profil">
                                                            <a href="">    <img style="width: 50px;height: 50px;"
                                                                <?php
                                                                if ($userPost['post_img'] == NULL) {
                                                                    $img_prof = "http://" . HOMES . "/" . FOLDER_FILE . "/media/img/img_profil/icon_not_found.png";
                                                                } else {
                                                                    $img_prof = "http://" . HOMES . "/" . FOLDER_FILE . "/" . $userPost['post_img'];
                                                                }
                                                                ?>src="<?php echo $img_prof; ?>">
                                                            </a>
                                                        </div></div>
                                                    <div class="col-md-11"> 
                                                        <div class="wrapper_profil" style="    height: 50px;
                                                             margin: 5px;
                                                             text-align: left;
                                                             margin-left: -20px;">
                                                            <div style="padding: 5px;
                                                                 margin-top: 5px;
                                                                 padding-bottom: 0px;
                                                                 text-align: left;
                                                                 font-family: 'Roboto',sans-serif;
                                                                 font-size: 15px;
                                                                 text-align: left;
                                                                 font-weight: bold;" class="name_profil">  <?php echo $userPost['nama']; ?> </div>
                                                            <div style="padding: 5px;
                                                                 margin-top: -5px;
                                                                 padding-top: : 0px;
                                                                 text-align: left;
                                                                 font-family: 'Roboto',sans-serif;
                                                                 font-size: 12px;
                                                                 text-align: left;
                                                                 color: gray;"class="addres_profil">  <?php echo $userPost['alamat']; ?> </div>
                                                        </div>
                                                    </div>
                                                </div>    

                                                <div style="padding:5px; padding-bottom: 0px; text-align:left ;font-family: 'Roboto',sans-serif;font-size: 30px;" class="title-qlue">   <?php echo $userPost['post_title']; ?> </div>
                                                <div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 15px; text-align: left; color: green" class="title-qlue"> Gaji :   Rp. 3.400.000,- </div>
                                            </div>

                                        </div>
                                        <div class="img-clue">
                                            <!--<img style="width: 100%;"src="--> 
                                            <?php
//                                                if ($userPost['post_img'] == NULL) {
//                                                    $img_prof = "./media/img/img_post/image-not-found.png";
//                                                }
//                                                else {
//                                                    $img_prof = $userPost['post_img'];
//                                                }
                                            ?><?php // echo $img_prof; ?> 
                                            <!--">-->
                                        </div>
                                        <div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 14px;" class="desc-clue"> <?php echo $userPost['post_desc']; ?></div>
                                        <span id='btn-lamar'></span>
                                        <?php
                                        $data = $mydatabase->myquery($q_apply);
                                        $url = $mydatabase->carr_get($myresult, 'post_url');
                                        if($check_session){if (count($data) == 0) {
                                            echo "<br><div class='btn btn-primary'><a style='color:#fff;' href='http://" . HOMES . '/' . FOLDER_FILE . "/post/setlamar.php?url=" . $url . "&id=" . $code . "'>Lamar Sekarang</a></div>";
                                        } else {
                                            echo "<br><div class='btn btn-success'>Sudah melamar diperusahaan ini</div>";
                                        }} else {
    echo "<br><div class='btn btn-primary'><a style='color:#fff;' href='http://" . HOMES . '/' . FOLDER_FILE . "/post/setlamar.php?url=" . $url . "&id=" . $code . "'>Lamar Sekarang</a></div>";
}
                                        ?>                     

                                    </div>
                                    <?php
                                }
// End dari content
                                ?>
                            </div>
                        </div>

                    </div>
  
                </div>
            </div>

        </div>

        <?php include FOOTER; ?>

    </body>
</html>
