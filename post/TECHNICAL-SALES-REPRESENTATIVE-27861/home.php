<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
$check_session = $mydatabase->check_session();
if ($check_session) {
    $userRow_data = $mydatabase->user_log($_SESSION['user']);
    $userRowProfil = $userRow_data[0];
}


// Get code POST
$code = $mydatabase->get_code_post();

//Query post
$q = "SELECT mp.*, c.nama,c.alamat FROM post mp
inner join company c
on c.company_id = mp.company_id
where mp.status > 0 and mp.member_post_id = " . $code . " order by mp.member_post_id DESC";
$myresult = $mydatabase->myquery($q);

?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $mydatabase->carr_get($myresult,'post_title');?> </title>
<?php include HEAD_SETTING; ?>
    </head>
    <body>

<?php include NAVIGATION; ?>

        <div id="wrapper">

            <div style="background-color: #fff; padding-bottom: 50px"class="container">

                <div class="page-header margin-top-10">
                    <div class="row"> <div class="col-md-6 text-align-md">   	<h3 style="font-family: Roboto,sans-serif;
                                                                                  font-weight: bold;">Daftar lowongan kerja 2017</h3></div>

                        <div style="text-align: right;" class="hidden-xs hidden-sm  col-md-6 margin-top-10">
                            
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 main-bar">
                        <div class="main-content">
                            <div class="row">
<?php
// Content
foreach ($myresult as $key => $userPost) {
    ?>
                                    <div class="col-md-12 qlue-content"> 
                                        <div style="border:1px solid #f3f5f6;border-radius: 1%;"class="wrapper-qlue">

                                            <div style="text-align: center; margin-bottom: 10px;" class="head-clue">
                                                <div class="row">
                                                    <div class="col-xs-3 col-md-1"><div style="width: 50px;

                                                                                        height: 50px;   
                                                                                        margin: 5px;"class="img-profil">
                                                            <a href="">    <img style="width: 50px;height: 50px;"
    <?php
    if ($userPost['post_img'] == NULL) {
        $img_prof = "http://" . HOMES . "/" . FOLDER_FILE . "/media/img/img_profil/icon_not_found.png";
    } else {
        $img_prof = "http://" . HOMES . "/" . FOLDER_FILE . "/" . $userPost['post_img'];
    }
    ?>src="<?php echo $img_prof; ?>">
                                                            </a>
                                                        </div></div>
                                                    <div class="col-md-11"> 
                                                        <div class="wrapper_profil" style="    height: 50px;
                                                             margin: 5px;
                                                             text-align: left;
                                                             margin-left: -20px;">
                                                            <div style="padding: 5px;
                                                                 margin-top: 5px;
                                                                 padding-bottom: 0px;
                                                                 text-align: left;
                                                                 font-family: 'Roboto',sans-serif;
                                                                 font-size: 15px;
                                                                 text-align: left;
                                                                 font-weight: bold;" class="name_profil">  <?php echo $userPost['nama']; ?> </div>
                                                            <div style="padding: 5px;
                                                                 margin-top: -5px;
                                                                 padding-top: : 0px;
                                                                 text-align: left;
                                                                 font-family: 'Roboto',sans-serif;
                                                                 font-size: 12px;
                                                                 text-align: left;
                                                                 color: gray;"class="addres_profil">  <?php echo $userPost['alamat']; ?> </div>
                                                        </div>
                                                    </div>
                                                </div>    

                                                <div style="padding:5px; padding-bottom: 0px; text-align:left ;font-family: 'Roboto',sans-serif;font-size: 30px;" class="title-qlue">   <?php echo $userPost['post_title']; ?> </div>
                                                <div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 15px; text-align: left; color: green" class="title-qlue"> Gaji :   Rp. 3.400.000,- </div>
                                            </div>

                                        </div>
                                        <div class="img-clue">
                                            <!--<img style="width: 100%;"src="--> 
    <?php
//                                                if ($userPost['post_img'] == NULL) {
//                                                    $img_prof = "./media/img/img_post/image-not-found.png";
//                                                }
//                                                else {
//                                                    $img_prof = $userPost['post_img'];
//                                                }
    ?><?php // echo $img_prof; ?> 
                                            <!--">-->
                                        </div>
                                        <div style="padding: 5px;padding-bottom: 0px;text-align: left;font-family: 'Roboto',sans-serif;font-size: 14px;" class="desc-clue"> <?php echo $userPost['post_desc']; ?></div>
                                    </div>
    <?php
}
// End dari content
?>
                            </div>
                        </div>

                    </div>
                   
                </div>
            </div>

        </div>

<?php include FOOTER; ?>

    </body>
</html>
