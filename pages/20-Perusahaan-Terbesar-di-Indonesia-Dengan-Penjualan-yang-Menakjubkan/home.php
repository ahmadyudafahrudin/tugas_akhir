<?php
session_start();
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
$check_session = $mydatabase->check_session();
if ($check_session) {
    $userRow_data = $mydatabase->user_log($_SESSION['user']);
    $userRowProfil = $userRow_data[0];
}
// Get code POST
$code = $mydatabase->get_code_post();

//Query post
$q = "SELECT * FROM navigation where navigation_id=" . $code;
$myresult = $mydatabase->myquery($q);
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $mydatabase->carr_get($myresult, 'nama'); ?></title>
        <?php include HEAD_SETTING; ?>
    </head>
    <body>

        <?php include NAVIGATION; ?>

        <div id="wrapper">

            <div style="background-color: #fff; padding-bottom: 50px"class="container">

                <div class="page-header margin-top-10">
                    <div class="row"> <div class="col-md-6 text-align-md">   	<h3 style="font-family: Roboto,sans-serif; text-transform: uppercase;
                                                                                  font-weight: bold;"><?php echo $mydatabase->carr_get($myresult, 'nama'); ?></h3></div>

                        <div style="text-align: right;" class="hidden-xs hidden-sm  col-md-6 margin-top-10">
                            
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 main-bar">
                        <div class="main-content">
                            <div class="row">
                                <?php
// Content
                                foreach ($myresult as $key => $userPost) {
                                    ?>
                                    <div class="col-md-12 qlue-content"> 
                                        <div style="border-radius: 1%;"class="wrapper-qlue">
<?php 
    echo $userPost['isi'];
?>

                                        </div>
                                    </div>
                                    <?php
                                }
// End dari content
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include FOOTER; ?>

    </body>
</html>
