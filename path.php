<?php
// PATH DATABASE
define('DBHOST', 'localhost');
define('DBUSER', 'root');
define('DBPASS', '');

// PATH SERVER
define('HOMES', $_SERVER['SERVER_NAME']);
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

// PATH FOLDER
define('FOLDER_FILE', 'tugas_akhir');


//--------------------- HEAD ---------------------//
$file = ROOT . '/header/head.php';
define('HEAD_SETTING', $file);

define('META_DESCRIPTION', '');
define('META_KEYWORD', '');

//--------------------- HEAD ---------------------//

//--------------------- BANNER ---------------------//
    define('PATH_BANNER', 'media/img/banner/');
//--------------------- BANNER ---------------------//

//--------------------- NAV ---------------------//
$file_2 = ROOT . '/header/nav.php';
define('NAVIGATION', $file_2);
$file_44 = ROOT . '/header/nav_login.php';
define('NAVIGATION_LOGIN', $file_44);
$file_444 = ROOT . '/header/nav_search.php';
define('NAVIGATION_SEARCH', $file_444);
//--------------------- NAV ---------------------//

//--------------------- FOT ---------------------//
$file_3 = ROOT . '/footer/fot.php';
define('FOOTER', $file_3);
//--------------------- FOT ---------------------//

?>