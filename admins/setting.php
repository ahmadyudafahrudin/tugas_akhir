<!DOCTYPE html>

<?php
    session_start();
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
    $check_session = $mydatabase->admin_check_session();
    if ($check_session) {
        $userRow_data = $mydatabase->admin_log($_SESSION['admin']);
        $userRowProfil = $userRow_data[0];
    }

    $request = $_GET;
    $page = '';
    if (isset($request['pages'])) {
        $page = $request['pages'] . '.php';
    }
    else {
        $page = 'logo.php';
    }
?>
<html>
    <head>
        <title>Admin</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="<?php echo 'http://' . HOMES . '/media/ckeditor/'; ?>ckeditor.js"></script>
        <script src="js/sample.js"></script>
        <link rel="stylesheet" href="toolbarconfigurator/lib/codemirror/neo.css">
    </head>
    <body>

        <div id="container">

            <div id="menu">
                <a class="left " target="_blank" href="http://<?php echo HOMES; ?>"><img src="images/home.png">Lihat WEB</a>
                <a class="center" href="./"><img src="images/home.png">Data</a>
                <a class="center selected" href="./setting.php"><img src="images/menu5.png">Setting</a>
                <a class="right" href="./logout/?logout"><img src="images/menu4.png">Logout</a>

            </div>

            <div id="shortcut">
                <a href="<?php echo './setting.php?pages=logo' ?>"><img src="images/short1.png"><br>Logo</a>
                <a href="<?php echo './setting.php?pages=navigasi' ?>"><img src="images/short1.png"><br>Navigasi Menu</a>
                <a href="<?php echo './setting.php?pages=cms_side' ?>"><img src="images/short1.png"><br>CMS Side</a>
            </div>

            <?php
                include $page;
            ?>

            <div id="footer">
                Copyright &copy; 2013 | Admin Template<br>
                Create & Design by TutorialWeb.Net
            </div>
        </div>

    </body>
</html>