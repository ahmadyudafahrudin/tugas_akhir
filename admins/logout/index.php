<?php

session_start();

include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// it will never let you open index(login) page if session is set
if (!isset($_SESSION['admin']) != "") {
    header("Location: http://" . HOMES . "/admins/login");
    exit;
}

if (isset($_GET['logout'])) {
    unset($_SESSION['admin']);
    session_unset();
    session_destroy();
    header("Location: http://" . HOMES . "/admins/login");
    exit;
}