<?php
    $disable = FALSE;
    $edit = FALSE;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $disable = TRUE;
        $edit = TRUE;
        $q_isi = "select * from navigation where navigation_id=" . $id;
        $isi = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'isi');
        $nama = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'nama');
    }
    $q = "select * from company where status > 0";

    $admin_id = $mydatabase->carr_get($userRow_data, 'id');
    $data_perusahaan = $mydatabase->myquery($q);
?>
<div id="content">
    <center>
        <table border="0" width="60%" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td width="100%" style="padding-right:20px;">

                    <div id="body">
                        <div class="title">Buat Post</div>
                        <div class="body">
                            <form enctype="multipart/form-data" action="set_post.php<?php
                                if ($disable) {
                                    echo '?action=edit&id=' . $id;
                                }
                            ?>" method="post">
                                <table>
                                    <tr>
                                        <td><b>Judul</b><div class="desc">Nama yang ditampilkan menu</div></td>
                                        <td>:</td>
                                        <td><input <?php
                                                if ($disable) {
                                                    echo 'disabled value="' . $nama . '"';
                                                }
                                            ?> type="text" name="nama" required /></td>
                                    </tr>
                                    <tr>
                                        <td><b>Perusahaan</b><div class="desc">pilih perusahaan</div></td>
                                        <td>:</td>
                                        <td><select <?php
                                                if ($disable) {
                                                    echo 'disabled';
                                                }
                                            ?> name="id_perusahaan">
                                                    <?php
                                                        foreach ($data_perusahaan as $key => $value) {
                                                            ?>
                                                        <option value="<?php echo $value['company_id'] ?>"><?php echo $value['nama'] ?></option>
                                                    <?php }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td><b>Gambar</b><div class="desc">Pilih gambar</div></td>
                                        <td>:</td>
                                        <td><input style="margin-bottom:10px;" type="file" name="file" id="file" required />
                                            <input type="hidden" name="admin_id" value="<?php echo $admin_id; ?>" />
                                        </td>
                                    </tr>


                                    <tr>
                                        <td><b>Isi</b><div class="desc">Isikan deskripsi Anda</div></td>
                                        <td>:</td>
                                        <td>

                                            <div class="adjoined-bottom">
                                                <div class="grid-container">
                                                    <div class="grid-width-100">
                                                      <textarea name="isi" id="editor1">&lt;p&gt;Initial editor content.&lt;/p&gt;</textarea>

                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" value="Simpan" /></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table border="0" width="100%" cellpadding="0" cellspacing="0">

            <tr valign="top">
                <td colspan="3">
                    <table class="table" width="100%">
                        <tr class="th">
                            <th width="3%">No.</th>
                            <th>Web</th>
                            <th>Username Penulis</th>
                            <th>Judul</th>
                            <th width="20%">Gambar</th>
                            <th>Isi</th>
                            <th>Post url</th>
                            <th>Status</th>
                            <th width="10%">Setting</th>
                        </tr>
                        <?php
                            $number = 0;
                            $status = 'non active';
                            $q = "select p.*, a.username as admin_user,p.status as poststatus, md.*, m.* from post p "
                                    . "left join member_detail md on md.member_detail_id = p.member_detail_id "
                                    . "left join member m on m.id = p.member_detail_id "
                                    . "left join admin a on a.id = p.admin_id ";
                            $data = $mydatabase->myquery($q);

                            foreach ($data as $key => $value) {
                                $number++;
                                if ($value['poststatus'] == 0) {

                                    $status = 'Non active';
                                    $status_action = 'Active';
                                    $path = 'postactive';
                                }
                                else {
                                    $status = 'Active';
                                    $status_action = 'Non active';
                                    $path = 'postnonactive';
                                }
                                if ($value['username'] == NULL) {
                                    $username = $value['admin_user'];
                                }
                                else {
                                    $username = $value['username'];
                                }
                                echo '
                                
                                
                                <tr class = "td" bgcolor = "#FFF">
                                <td align = "center">' . $number . '</td>
                                <td>' . $value['org_tema_id'] . '</td>
                                    <td>' . $username . '</td>
                                <td>' . $value['post_title'] . '</td>
                                <td><img class="img-responsive" style="width:100px;" src="http://' . HOMES . '/' . $value['post_img'] . '"></td>
                                <td>' . $value['post_desc'] . '</td>
                                <td>' . $value['post_url'] . '</td>
                                <td>' . $status . '</td>
                                 <td align = "center">' . $status_action . '<a href="./' . $path . '.php?id=' . $value['member_post_id'] . '"><img src="images/delete.png"></a></td>
                                </tr>
                                   
                                    ';
                            }
                        ?>
                    </table>
                </td>
            </tr>
        </table>
<script>
    CKEDITOR.replace( 'editor1' );
</script>
</div>
