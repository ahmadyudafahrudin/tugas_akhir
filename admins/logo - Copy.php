<center><div id="content">
        	<table border="0" width="50%" cellpadding="0" cellspacing="0">
            	<tr valign="top">
                	<td width="100%" style="padding-right:20px;">
                		<div id="body">
                        	<div class="title">Contoh Form</div>
                            <div class="body">
                            	<form action="" method="post">
                                <table>
                                	<tr>
                                    	<td><b>Nama Lengkap</b><div class="desc">Masukkan nama lengkap</div></td>
                                        <td>:</td>
                                        <td><input type="text" required /></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Email</b><div class="desc">Masukkan Email yang Valid</div></td>
                                        <td>:</td>
                                        <td><input type="text" required /></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Username</b><div class="desc">Minimal 4 karakter</div></td>
                                        <td>:</td>
                                        <td><input type="text" required /></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Password</b><div class="desc">Minimal 6 karakter</div></td>
                                        <td>:</td>
                                        <td><input type="text" required /></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Ulangi Password</b><div class="desc">Ulangi password di atas</div></td>
                                        <td>:</td>
                                        <td><input type="text" required /></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Jenis Kelamin</b><div class="desc">Pilih jenis kelamin</div></td>
                                        <td>:</td>
                                        <td><select><option>Pilih Salah Satu</option><option>Laki-Laki</option><option>Perempuan</option></select></td>
                                    </tr>
                                    <tr>
                                    	<td><b>Agama</b><div class="desc">Pilih Agama Anda</div></td>
                                        <td>:</td>
                                        <td>
                                        	<input type="radio" name="agama" required>Islam
                                            <input type="radio" name="agama" required>Hinda
                                            <input type="radio" name="agama" required>Kristen
                                            <input type="radio" name="agama" required>Budha
                                            <input type="radio" name="agama" required>Katholik
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td><b>Hobby</b><div class="desc">Pilih beberapa hobby</div></td>
                                        <td>:</td>
                                        <td>
                                        	<input type="checkbox">Makan
                                            <input type="checkbox">Minum
                                            <input type="checkbox">Mancing
                                            <input type="checkbox">Liburan
                                            <input type="checkbox">Komputer
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td><b>Alamat</b><div class="desc">Alamat lengkap anda</div></td>
                                        <td>:</td>
                                        <td><textarea rows="5" cols="50"></textarea></td>
                                    </tr>
                                    <tr>
                                    	<td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" value="Simpan" /><a class="orange" href="#"><img src="images/button-cancel.png">Batal</a></td>
                                    </tr>
                                </table>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </center>
        </div>