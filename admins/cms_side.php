<?php
    $disable = FALSE;
    $edit = FALSE;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $disable = TRUE;
        $edit = TRUE;
        $q_isi = "select * from cms_side where cms_side_id=" . $id;
        $isi = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'deskripsi');
        $nama = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'nama');
        $type = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'type_content');
        $max = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'max_content');
    }
?>
<center>
    <div id="content">
        <table border="0" width="60%" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td width="100%" style="padding-right:20px;">
                    <div id="body">
                        <div class="title">Cms Side</div>
                        <div class="body">
                            <form action="set_cms_side.php<?php
                                if ($disable) {
                                    echo '?action=edit&id=' . $id;
                                }
                            ?>" method="post">
                                <table>
                                    <tr>
                                        <td><b>Nama CMS</b><div class="desc">Nama di side menu</div></td>
                                        <td>:</td>
                                        <td><input <?php
                                                if ($disable) {
                                                    echo 'disabled value="' . $nama . '"';
                                                }
                                            ?> type="text" name="nama" required /></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type</b><div class="desc">Pilih content</div></td>
                                        <td>:</td>
                                        <td><select <?php
                                                if ($disable) {
                                                    echo 'disabled';
                                                }
                                            ?> name="type">
                                                    <?php
                                                        if ($disable) {
                                                            echo '<option selected>' . $type . '</option>';
                                                        }
                                                    ?>
                                                <option>Side</option>
                                                <option>Html</option>
                                            </select></td>
                                    </tr>

                                    <tr>
                                        <td><b>Max Tampilan</b><div class="desc">Maksimkal yang ditampilakan</div></td>
                                        <td>:</td>
                                        <td><input <?php
                                                if ($disable) {
                                                    echo 'disabled value="' . $max . '"';
                                                }
                                            ?> type="text" name="max_content" required /></td>
                                    </tr>

                                    <tr>
                                        <td><b>Deskripsi</b><div class="desc">isikan deskripsi cms side</div></td>
                                        <td>:</td>
                                        <td>
                                            <div class="adjoined-bottom">
                                                <div class="grid-container">
                                                    <div class="grid-width-100">
                                                        <textarea name="isi" id="editor1">&lt;p&gt;Initial editor content.&lt;/p&gt;</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Html</b><div class="desc">isikan jika type HTML</div></td>
                                        <td>:</td>
                                        <td>

                                            <div class="adjoined-bottom">
                                                <div class="grid-container">
                                                    <div class="grid-width-100">
                                                        <textarea name="html" id="editor2">&lt;p&gt;Initial editor content.&lt;/p&gt;</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" value="Simpan" /></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table" width="100%">
            <tr class="th">
                <th width="3%">No.</th>
                <th>Cms side</th>
                <th>Type</th>
                <th>Status</th>
                <th>Aksi</th>
                <th>Aksi</th>
            </tr>

            <?php
                $q = "select * from cms_side ";
                $data = $mydatabase->myquery($q);
                $number = 0;
                foreach ($data as $key => $value) {
                    $number++;
                    if ($value['status'] == 0) {

                        $status = 'Non active';
                        $status_action = 'Active';
                        $path = 'active_cms';
                    }
                    else {
                        $status = 'Active';
                        $status_action = 'Non active';
                        $path = 'nonactive_cms';
                    }

                    echo '
                                
                                
                                <tr class = "td" bgcolor = "#FFF">
                                <td align = "center">' . $number . '</td>
                                <td align = "center">' . $value['nama'] . '</td>
                                <td align = "center">' . $value['type_content'] . '</td>
                                <td align = "center">' . $status . '</td>
                                <td align = "center">' . $status_action . '<a href="./' . $path . '.php?id=' . $value['cms_side_id'] . '"><img src="images/delete.png"></a></td>
                                <td align = "center">Edit<a href="./setting.php?pages=cms_side&id=' . $value['cms_side_id'] . '"><img src="images/edit.png"></a></td>
                               </tr>
                                   
                                    ';
                }
            ?>
        </table>
    </div>
</center>
<script>
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
</script>
</div>