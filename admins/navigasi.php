<?php
    $disable = FALSE;
    $edit = FALSE;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $disable = TRUE;
        $edit = TRUE;
        $q_isi = "select * from navigation where navigation_id=" . $id;
        $isi = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'isi');
        $nama = $mydatabase->carr_get($mydatabase->myquery($q_isi), 'nama');
    }
?>
<center>
    <div id="content">
        <table border="0" width="60%" cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td width="100%" style="padding-right:20px;">
                    <div id="body">
                        <div class="title">Navigasi Menu</div>
                        <div class="body">
                            <form action="set_navigasi.php<?php
                                if ($disable) {
                                    echo '?action=edit&id=' . $id;
                                }
                            ?>" method="post">
                                <table>
                                    <tr>
                                        <td><b>Nama navigasi</b><div class="desc">Nama yang ditampilkan menu</div></td>
                                        <td>:</td>
                                        <td><input <?php
                                if ($disable) {
                                    echo 'disabled value="' . $nama . '"';
                                }
                            ?> type="text" name="nama" required /></td>
                                    </tr>
                                    <tr>
                                        <td><b>Type</b><div class="desc">Pilih type navigasi</div></td>
                                        <td>:</td>
                                        <td><select <?php
                                if ($disable) {
                                    echo 'disabled';
                                }
                            ?> name="type">
                                                <option>Pages</option>
                                                <option>Link</option>
                                                <option>Side</option>
                                            </select></td>
                                    </tr>


                                    <tr>
                                        <td><b>Isi</b><div class="desc">Isikan Pages Anda</div></td>
                                        <td>:</td>
                                        <td>
                                            <div class="adjoined-bottom">
                                                <div class="grid-container">
                                                    <div class="grid-width-100">
                                                        <textarea name="isi" id="editor1">&lt;p&gt;Initial editor content.&lt;/p&gt;</textarea>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td><input type="submit" value="Simpan" /></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table" width="100%">
            <tr class="th">
                <th width="3%">No.</th>
                <th>Nama navigasi menu</th>
                <th>Url</th>
                <th>Type</th>
                <th>Status</th>
                <th>Aksi</th>
                <th>Aksi</th>
            </tr>

            <?php
                $q = "select * from navigation ";
                $data = $mydatabase->myquery($q);
                $number = 0;
                foreach ($data as $key => $value) {
                    $number++;
                    if ($value['status'] == 0) {

                        $status = 'Non active';
                        $status_action = 'Active';
                        $path = 'navigasiactive';
                    }
                    else {
                        $status = 'Active';
                        $status_action = 'Non active';
                        $path = 'navigasinonactive';
                    }

                    echo '
                                
                                
                                <tr class = "td" bgcolor = "#FFF">
                                <td align = "center">' . $number . '</td>
                                <td align = "center">' . $value['nama'] . '</td>
                                <td align = "center"><a href="' . $value['url'] . '">' . $value['nama'] . '</a></td>
                                <td align = "center">' . $value['type'] . '</td>
                                <td align = "center">' . $status . '</td>
                                    <td align = "center">' . $status_action . '<a href="./' . $path . '.php?id=' . $value['navigation_id'] . '"><img src="images/delete.png"></a></td>
                                        <td align = "center">Edit<a href="./setting.php?pages=navigasi&id=' . $value['navigation_id'] . '"><img src="images/edit.png"></a></td>
                               </tr>
                                   
                                    ';
                }
            ?>
        </table>
    </div>
</center>
<script>
    CKEDITOR.replace( 'editor1' );
</script>
</div>