<!DOCTYPE html>

<?php
    session_start();
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/path.php');
    include ($_SERVER["DOCUMENT_ROOT"] . '/Tugas_akhir/db_config.php');

// select loggedin users detail
    $check_session = $mydatabase->admin_check_session();
    if ($check_session) {
        $userRow_data = $mydatabase->admin_log($_SESSION['admin']);
        $userRowProfil = $userRow_data[0];
    }

    $request = $_GET;
    $page = '';
    if (isset($request['pages'])) {
        $page = $request['pages'] . '.php';
    }
    else {
        $page = 'member.php';
    }
?>
<html>
    <head>
        <title>Admin</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="<?php echo 'http://' . HOMES . '/media/ckeditor/'; ?>ckeditor.js"></script>
        <script src="js/sample.js"></script>
        <link rel="stylesheet" href="toolbarconfigurator/lib/codemirror/neo.css">
    </head>
    <body>
        <div id="container">

            <div id="menu">
                <a class="left " target="_blank" href="http://<?php echo HOMES; ?>"><img src="images/home.png">Lihat WEB</a>
                <a class="center selected" href="./"><img src="images/home.png">Data</a>
                <a class="center" href="./setting.php"><img src="images/menu5.png">Setting</a>
                <a class="right" href="./logout/?logout"><img src="images/menu4.png">Logout</a>

            </div>
            <div style="position: absolute; top:18%;left: 20px" class="message"></div>
            <div id="shortcut">
                <a href="<?php echo './?pages=member' ?>"><img src="images/short1.png"><br>Member</a>
                <a href="<?php echo './?pages=posting' ?>"><img src="images/short1.png"><br>Posting</a>
            </div>

            <?php
                include $page;
            ?>

            <div id="footer">
                Copyright &copy; 2013 | Admin Template<br>
                Create & Design by TutorialWeb.Net
            </div>
        </div>

    </body>
</html>