
<div id="content">
    <table border="0" width="100%" cellpadding="0" cellspacing="0">

        <tr valign="top">
            <td colspan="3">
                <table class="table" width="100%">
                    <tr class="th">
                        <th width="3%">No.</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th width="20%">Nama Lengkap</th>
                        <th>Alamat</th>
                        <th>Status</th>
                        <th width="10%">Setting</th>
                    </tr>
                    <?php
                    $number = 0;
                    
                    $q = "select m.*, md.* from member m inner join member_detail md on md.member_detail_id = m.id";
                    $data = $mydatabase->myquery($q);

                    foreach ($data as $key => $value) {
                        $number++;
                        if ($value['status'] == 0) {
                            
                            $status = 'Non active';
                            $status_action = 'Active';
                            $path = 'active';
                        }else{
                            $status = 'Active';
                            $status_action = 'Non active';
                            $path = 'nonactive';
                            
                        }
                        echo '
                                
                                
                                <tr class = "td" bgcolor = "#FFF">
                                <td align = "center">' . $number . '</td>
                                <td>' . $value['username'] . '</td>
                                <td>' . $value['email'] . '</td>
                                <td>' . $value['nama'] . '</td>
                                <td>' . $value['alamat'] . '</td>
                                <td>' . $status . '</td>
                                <td align = "center">'.$status_action.'<a href="./'.$path.'.php?id=' . $value['id'] . '"><img src="images/delete.png"></a></td>
                                </tr>
                                   
                                    ';
                    }
                    ?>
                </table>
            </td>
        </tr>
    </table>
</div>